﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Entities.DTO.Employee;
using Entities.Models;
using HomeCareContracts;
using Microsoft.AspNetCore.Mvc;

namespace HomeCareWebApi.Controllers
{
    [Route("api/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        public EmployeeController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper) 
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllEmployees()
        {
            try
            {
                var employees = await _repository.employee.GetAllEmployeesAsync();
                _logger.LogInfo($"Returned all employees from database.");

                var employeesResults = _mapper.Map<IEnumerable<EmployeeDTO>>(employees);
                employeesResults.ToList();
                return Ok(employeesResults);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllEmployees action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}/FirstName")]
        public async Task<IActionResult> GetDetailsEmployee(int id)
        {
            try
            {
                var employee = await _repository.employee.GetEmployeeWhitDetailsAsync(id);

                if (employee == null)
                {
                    _logger.LogInfo($"Returned one with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned employee with id: {id}");
                    var employeeResult = _mapper.Map<EmployeeDTO>(employee);
                    return Ok(employeeResult);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetDetailsEmployee action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpGet("{id}", Name = "GetEmployeeById")]
        public async Task<ActionResult> GetEmployeeById(int id)
        {
            try
            {
                var employee = await _repository.employee.GetEmployeeByIdAsync(id);

                if (employee == null)
                {
                    _logger.LogInfo($"Returned one with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned employee with id: {id}");
                    var employeeResult = _mapper.Map<EmployeeDTO>(employee);
                    return Ok(employeeResult);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetEmployeeById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpPost]
        public IActionResult CreateEmployee([FromBody] EmployeeForCreationDTO employee)
        {
            try
            {
                if (employee == null)
                {
                    _logger.LogError("Employee object sent from employee is null.");
                    return BadRequest("Employee object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Employee object sent from Employee.");
                    return BadRequest("Invalid model object");
                }
                var employeeEntity = _mapper.Map<Employee>(employee);
                _repository.employee.CreateEmployee(employeeEntity);
                _repository.save();
                var createEmployee = _mapper.Map<EmployeeDTO>(employeeEntity);

                return CreatedAtRoute("GetEmployeeById", new { id = createEmployee.IdEmployees }, createEmployee);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateEmployee action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateEmployee(int id, [FromBody] EmployeeForUpdateDTO employee)
        {
            try
            {
                if (employee == null)
                {
                    _logger.LogError("The Employee object sent from employee is null.");
                    return BadRequest("Employee object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Employee object sent from client.");
                    return BadRequest();
                }
                var employeeEntity = await _repository.employee.GetEmployeeByIdAsync(id);
                if (employeeEntity == null)
                {
                    _logger.LogError("$User with id: { id}, hasn't been found in db.");
                    return NotFound();
                }

                _mapper.Map(employee, employeeEntity);
                _repository.employee.UpdateEmployee(employeeEntity);
                _repository.save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateEmployee action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee(int id)
        {
            try
            {
                var employee = await _repository.employee.GetEmployeeByIdAsync(id);
                if (employee == null)
                {
                    _logger.LogError($"Employee with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.employee.DeleteEmployee(employee);
                _repository.save();
                return NoContent();

            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteEmployee action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
