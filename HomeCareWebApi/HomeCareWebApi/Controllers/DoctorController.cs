﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Entities.DTO.Doctor;
using Entities.Models;
using HomeCareContracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HomeCareWebApi.Controllers
{
    [Route("api/doctor")]
    [ApiController]
    public class DoctorController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        public DoctorController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllDoctors()
        {
            try
            {
                var doctors = await _repository.doctor.GetAllDoctorsAsync();
                _logger.LogInfo($"Returned all users from database.");

                var doctorsResults = _mapper.Map<IEnumerable<DoctorDTO>>(doctors);
                doctorsResults.ToList();
                return Ok(doctorsResults);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllUsers action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpGet("{id}/FirstName")]
        public async Task<IActionResult> GetDetailsDoctor(int id)
        {
            try
            {
                var doctor = await _repository.doctor.GetDoctorWhitDetailsAsync(id);

                if (doctor == null)
                {
                    _logger.LogInfo($"Returned one with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned owner with id: {id}");
                    var doctorResult = _mapper.Map<DoctorDTO>(doctor);
                    return Ok(doctorResult);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetUserById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpGet("{id}", Name = "GetDoctorById")]
        public async Task<ActionResult> GetDoctorById(int id)
        {
            try
            {
                var doctor = await _repository.doctor.GetDoctorByIdAsync(id);

                if (doctor == null)
                {
                    _logger.LogInfo($"Returned one with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned owner with id: {id}");
                    var doctorResult = _mapper.Map<DoctorDTO>(doctor);
                    return Ok(doctorResult);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetUserById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public IActionResult CreateDoctor([FromBody] DoctorForCreationDTO doctor)
        {
            try
            {
                if (doctor == null)
                {
                    _logger.LogError("Doctor object sent from client is null.");
                    return BadRequest("Doctor object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Doctor object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var doctorEntity = _mapper.Map<Doctor>(doctor);
                _repository.doctor.CreateDoctor(doctorEntity);
                _repository.save();
                var createdDoctor = _mapper.Map<DoctorDTO>(doctorEntity);

                return CreatedAtRoute("GetDoctorById", new { id = createdDoctor.IdDoctor }, createdDoctor);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpPut ("{id}")]
        public async Task<IActionResult> UpdateDoctor(int id, [FromBody] DoctorForUpdateDTO doctor)
        {
            try
            {
                if (doctor == null)
                {
                    _logger.LogError("The Doctor object sent from client is null.");
                    return BadRequest("Doctor object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Doctor object sent from client.");
                    return BadRequest();
                }
                var doctorEntity = await _repository.doctor.GetDoctorByIdAsync(id);
                if (doctorEntity == null)
                {
                    _logger.LogError("$User with id: { id}, hasn't been found in db.");
                    return NotFound();
                }

                _mapper.Map(doctor, doctorEntity);
                _repository.doctor.UpdateDoctor(doctorEntity);
                _repository.save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDoctor(int id)
        {
            try
            {
                var doctor = await _repository.doctor.GetDoctorByIdAsync(id);
                if (doctor == null)
                {
                    _logger.LogError($"Doctor with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.doctor.DeleteDoctor(doctor);
                _repository.save();
                return NoContent();

            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
