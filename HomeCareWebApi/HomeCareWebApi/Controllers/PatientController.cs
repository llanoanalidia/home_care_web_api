﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Entities.DTO;
using Entities.DTO.Patient;
using Entities.Models;
using HomeCareContracts;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HomeCareWebApi.Controllers
{
    [Route("api/patient")]
    [ApiController]
    public class PatientController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;
       
        public PatientController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAllPatients ([FromQuery] PatientParameters patientParameters)
        {
            try
            {
                var patients =  _repository.patient.GetAllPatientsAsync(patientParameters);
              
                var metadata = new
                {
                    patients.TotalCount,
                    patients.PageSize,
                    patients.CurrentPage,
                    patients.TotalPages,
                    patients.HasNext,
                    patients.HasPrevious
                };
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
                _logger.LogInfo($"Returned { patients.TotalCount}from database.");
                
                var patientsResults = _mapper.Map<IEnumerable<PatientDTO>>(patients);
                patientsResults.ToList();
                return Ok(patientsResults);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllUsers action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpGet("{id}", Name = "GetPatientById")]
        public async Task<IActionResult> GetPatientById(int id)
        {
            try
            {
                var patient = await _repository.patient.GetPatienByIdAsync(id);

                if (patient == null)
                {
                    _logger.LogInfo($"Returned one with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned owner with id: {id}");
                    var patientResult = _mapper.Map<PatientDTO>(patient);
                    return Ok(patientResult);
                }
            }
            catch (Exception ex)
            {
                 _logger.LogError($"Something went wrong inside GetUserById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpGet("{id} /UserName")]
        public async Task<IActionResult> GetDetailsByPatient(int id) 
        {
            try
            {
                var patient = await _repository.patient.GetPatientWhitDetailsAsync(id);

                if (patient == null)
                {
                    _logger.LogInfo($"Returned one with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned owner with id: {id}");
                    var patientResult = _mapper.Map<PatientDTO>(patient);
                    return Ok(patientResult);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetUserById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpPost]
        public IActionResult CreatePatient([FromBody] PatientForCreationDTO patient)
        {
            try
            {
                if (patient == null)
                {
                    _logger.LogError("Patient object sent from client is null.");
                    return BadRequest("Patient object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Patient object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var patientEntity = _mapper.Map<Patient>(patient);
                _repository.patient.CreatePatient(patientEntity);
                _repository.save();
                var createdPatient = _mapper.Map<PatientDTO>(patientEntity);

                return CreatedAtRoute("GetPatientById", new { id = createdPatient.IdPatient }, createdPatient);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreatePatient action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePatient(int id)
        {
            try
            {
                var patient = await _repository.patient.GetPatienByIdAsync(id);
                if (patient == null)
                {
                    _logger.LogError($"Owner with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.patient.DeletePatient(patient);
                _repository.save();
                return NoContent();

            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePatient(int id, [FromBody] PatientForUpdateDTO patient) 
        {
            try
            {
                if (patient == null)
                {
                    _logger.LogError("The User object sent from client is null.");
                    return BadRequest("User object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid user object sent from client.");
                    return BadRequest();
                }
                var patientEntity = await _repository.patient.GetPatienByIdAsync(id);
                
                if (patientEntity == null)
                {
                    _logger.LogError("$User with id: { id}, hasn't been found in db.");
                    return NotFound();
                }

                _mapper.Map(patient, patientEntity);
                _repository.patient.UpdatePatient(patientEntity);
                _repository.save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
