﻿using AutoMapper;
using Entities.DTO;
using Entities.DTO.Demographic;
using Entities.DTO.Doctor;
using Entities.DTO.Employee;
using Entities.DTO.Patient;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeCareWebApi
{
    public class MappingProfile : Profile
    {
        public MappingProfile() 
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserForCreationDTO, User>();
            CreateMap<UserForUpdateDTO, User>();
            CreateMap<Patient, PatientDTO>();
            CreateMap<PatientForCreationDTO, Patient>();
            CreateMap<PatientForUpdateDTO, Patient>();
            CreateMap<Doctor, DoctorDTO>();
            CreateMap<DoctorForCreationDTO, Doctor>();
            CreateMap<DoctorForUpdateDTO, Doctor>();
            CreateMap<Demographic, DemographicDTO>();
            CreateMap<DemographicForCreationDTO, Demographic>();
            CreateMap<DemographicForUpdateDTO, Demographic>();
            CreateMap<Employee, EmployeeDTO>();
            CreateMap<EmployeeForCreationDTO, Employee>();
            CreateMap<EmployeeForUpdateDTO, Employee>();
        }
       

    }
}
