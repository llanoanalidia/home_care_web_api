﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HomeCareContracts
{
    public interface IAgencyRepository
    {
        Task<IEnumerable<Agency>> GetAllAgencysAsync();
        Task<Agency> GetAgencyByIdAsync(int id);
        Task<Agency> GetAgencyWhitDetailsAsync(int id);
        void CreateAgency(Agency agency);
        void UpdateAgency(Agency agency);
        void DeleteAgency(Agency agency);
    }
}
