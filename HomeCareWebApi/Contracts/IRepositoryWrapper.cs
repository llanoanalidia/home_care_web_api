﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCareContracts
{
   public interface IRepositoryWrapper
    {
        IUserRepository user { get; }
        IPatientRepository patient { get; }
        IDoctorRepository doctor { get; }

        IEmployeeRepository employee { get; }
        
        void save();
        
    }
}
