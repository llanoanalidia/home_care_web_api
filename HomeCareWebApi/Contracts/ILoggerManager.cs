﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCareContracts
{

    //Interfaz de Logger que se emplean metodos para el registro del log en el backend///
    public interface ILoggerManager
    {
        void LogInfo(string message);
        void LogWarn(string message);
        void LogDebug(string message);
        void LogError(string message);
    }
}
