﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HomeCareContracts
{
   public interface IInsuranceRepository
    {
        Task<IEnumerable<Insurance>> GetAllInsurancesAsync();
        Task<Insurance> GetInsuranceByIdAsync(int id);
        Task<Insurance> GetInsuranceWhitDetailsAsync(int id);
        void CreateInsurance(Insurance Insurance);
        void UpdateInsurance(Insurance Insurance);
        void DeleteInsurance(Insurance Insurance);
    }
}
