﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace HomeCareContracts
{
    public interface IPatientRepository
    {
        PagedList<Patient> GetAllPatientsAsync(PatientParameters patientParameters);
        Task<Patient> GetPatienByIdAsync(int id);
        Task<Patient> GetPatientWhitDetailsAsync(int id);
        void CreatePatient(Patient patient);
        void UpdatePatient(Patient patient);
        void DeletePatient(Patient patient);

    }
}
