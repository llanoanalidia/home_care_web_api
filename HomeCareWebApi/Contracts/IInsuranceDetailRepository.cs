﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HomeCareContracts
{
   public interface IInsuranceDetailRepository
    {
        Task<IEnumerable<InsuranceDetail>> GetAllInsuranceDetailsAsync();
        Task<InsuranceDetail> GetInsuranceDetailByIdAsync(int id);
        Task<InsuranceDetail> GetInsuranceDetailWhitDetailsAsync(int id);
        void CreateInsuranceDetail(InsuranceDetail insuranceDetail);
        void UpdateInsuranceDetail(InsuranceDetail insuranceDetail);
        void DeleteInsuranceDetail(InsuranceDetail insuranceDetail);
    }
}
