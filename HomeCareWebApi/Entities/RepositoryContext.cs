﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
   public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options)
            : base(options)
        {
        }
        public DbSet<User> User { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public  DbSet<Agency> Agency { get; set; }
        public  DbSet<Demographic> Demographic { get; set; }
        public  DbSet<Doctor> Doctor { get; set; }
        public  DbSet<Employee> Employee { get; set; }
        public  DbSet<Insurance> Insurance { get; set; }
        public  DbSet<InsuranceDetail> InsuranceDetail { get; set; }
        public  DbSet<Role> Role { get; set; }
    }
}
