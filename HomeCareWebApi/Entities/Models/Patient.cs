﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("Patient")]
    public partial class Patient
    {

        public Patient()
        {
            //InsuranceDetails = new HashSet<InsuranceDetail>();
            //Demographics = new HashSet<Demographic>();
        }

        [Key]
        public int IdPatient { get; set; }

        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(5)]
        public string Middle { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [StringLength(50)]
        public string SocialSecurityNumber { get; set; }

        [Required]
        [StringLength(10)]
        public string Gender { get; set; }

        [StringLength(20)]
        public string Phone1 { get; set; }

        [StringLength(20)]
        public string Phone2 { get; set; }

        [StringLength(100)]
        public string eMail { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public int UserId { get; set; }
        public int IdDemographic { get; set; }
        public string PatientCode { get; set; }

        [ForeignKey("IdDemographic")]
        public virtual Demographic Demographic { get; set; }
        
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        // 
        //public virtual ICollection < Demographic> Demographics { get; set; }
        //public virtual ICollection<InsuranceDetail> InsuranceDetails { get; set; }

    }
}
