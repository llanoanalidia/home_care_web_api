namespace Entities.Models
{
    using Entities.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Doctor")]
    public partial class Doctor
    {

        public Doctor() 
        {
            //Users = new HashSet<User>();

        }

        [Key]
        public int IdDoctor { get; set; }

        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string Middle { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [StringLength(50)]
        public string Phone1 { get; set; }

        [StringLength(50)]
        public string Phone2 { get; set; }

        [StringLength(50)]
        public string eMail { get; set; }

        [Required]
        [StringLength(100)]
        public string NPI { get; set; }
       
        
        public int IdUser { get; set; }
        
        [ForeignKey("IdUser")]
        public virtual User User { get; set; }
    }
}
