namespace Entities.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
   

    [Table("Insurance")]
    public partial class Insurance
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Insurance()
        {
            InsuranceDetails = new HashSet<InsuranceDetail>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InsuranceId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [Required]
        [StringLength(100)]
        public string Address1 { get; set; }

        [StringLength(100)]
        public string Address2 { get; set; }

        [Required]
        [StringLength(50)]
        public string City { get; set; }

        [Required]
        [StringLength(50)]
        public string State { get; set; }

        [Required]
        [StringLength(50)]
        public string ZipCode { get; set; }

        [Required]
        [StringLength(50)]
        public string PayorId { get; set; }

        [Required]
        [StringLength(100)]
        public string ClaimOfficeNumber { get; set; }

        [Required]
        [StringLength(10)]
        public string ClaimOfficeNumberType { get; set; }

        [Required]
        [StringLength(10)]
        public string MedigapID { get; set; }

        [Required]
        [StringLength(50)]
        public string CarrierCode { get; set; }

        [Required]
        [StringLength(10)]
        public string ProviderIdRef { get; set; }

        [Required]
        [StringLength(10)]
        public string TestProdIndicator { get; set; }

        public int ANSI { get; set; }

        public int SendSVT { get; set; }

        public int SendDecimalUnit { get; set; }

        [Required]
        [StringLength(5)]
        public string PaymentSrc { get; set; }

        public int CapitationPlan { get; set; }

        public decimal CapitationPayment { get; set; }

        public int DeleteFlag { get; set; }

        [Required]
        [StringLength(60)]
        public string Vmid { get; set; }

        [Required]
        [StringLength(30)]
        public string LabcorpId { get; set; }

        [Required]
        [StringLength(300)]
        public string Website { get; set; }

        public int Send2310EAlways { get; set; }

        public int ClaimSubmitType { get; set; }

        [Required]
        [StringLength(20)]
        public string ERAPayorId { get; set; }

        public int Inactive { get; set; }

        public int AllCodesCapitated { get; set; }

        public int SelCodesCapitated { get; set; }

        public int ReqOrdPrInfo { get; set; }

        public int SendDupSubNoForPat { get; set; }

        public int ProdDateAsCheckDate { get; set; }

        public int ReqUPINInEA021 { get; set; }

        [Required]
        [StringLength(11)]
        public string hl7id1 { get; set; }

        [Required]
        [StringLength(11)]
        public string hl7id2 { get; set; }

        [Required]
        [StringLength(50)]
        public string Class { get; set; }

        [Required]
        [StringLength(10)]
        public string TypeMedicare { get; set; }

        [Required]
        [StringLength(10)]
        public string TypeOthers { get; set; }

        public int FeeSchedId { get; set; }

        public int SecClaimsElectronic { get; set; }

        [Required]
        [StringLength(5)]
        public string PrAssignInd { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Notes { get; set; }

        [Required]
        [StringLength(100)]
        public string Box31Format { get; set; }

        public int Box17ProviderType { get; set; }

        [Required]
        [StringLength(60)]
        public string Box17ProviderIdType { get; set; }

        public int Box17ProviderRuleEnabled { get; set; }

        public int SendInsPinForSupr { get; set; }

        [Required]
        [StringLength(50)]
        public string Type { get; set; }

        public int ClaimType { get; set; }

        [Required]
        [StringLength(60)]
        public string Box31Line2Format { get; set; }

        public int SendSubDemAlways { get; set; }

        public int SplitClaimOnICD { get; set; }

        public int EnableReferralNumber { get; set; }

        public int DoNotPrintHcfa29 { get; set; }

        public int DoNotPrintHcfa30 { get; set; }

        [Required]
        [StringLength(10)]
        public string Box17ProviderIdTypeMap { get; set; }

        [Required]
        [StringLength(5)]
        public string McaidNYPlanCode { get; set; }

        public int PrintHcfa22 { get; set; }

        public int EnableSympDate { get; set; }

        public int SupressFacAddrForPOS11Enic { get; set; }

        public int UseNpi { get; set; }

        public int UseLegacy { get; set; }

        [Required]
        [StringLength(30)]
        public string X270payorId { get; set; }

        public int UseGroupNoForEligibility { get; set; }

        public int CalculateAllowed { get; set; }

        public int SupressFacAddrForPOS11Paper { get; set; }

        public int X270LegacyNPI { get; set; }

        public int FirstLinePost { get; set; }

        [Required]
        [StringLength(30)]
        public string HCFA_Box19 { get; set; }

        public int SendGrpNoin2310B { get; set; }

        public int SendOTAFSeg { get; set; }

        public int Send2420CAlways { get; set; }

        public int Send2420CIf2310DOmited { get; set; }

        public int UseLegacyForPrin { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Alert { get; set; }

        public int UseNPIForPrint { get; set; }

        public int PrintEPSDTReferralCodeInHcfa24C { get; set; }

        [Required]
        [StringLength(50)]
        public string SubscriberMask { get; set; }

        public int SubscriberLength { get; set; }

        public int SendStrictDOS { get; set; }

        public int RequiredReferral { get; set; }

        public int FeeSchedulesByLocation { get; set; }

        public int UseFacilityPrvForRendProv { get; set; }

        public int SendP2ProvPrvAlways { get; set; }

        public int FPStringMandatory { get; set; }

        public int SubNoMaxLen { get; set; }

        public int SubNoMinLen { get; set; }

        [Required]
        [StringLength(10)]
        public string PrimaryType { get; set; }

        public int ExcludeInPtStmts { get; set; }

        public int UseExtendedPrvOption { get; set; }

        public int Send2000APrv { get; set; }

        public int Send2310BPrv { get; set; }

        public int Send2420APrv { get; set; }

        public int SuppressRefProvUPIN { get; set; }

        public int SuppressTaxIdInLoop2310A { get; set; }

        public int SuppressTaxIdInLoop2310B { get; set; }

        public int SuppressTaxIdInLoop2420A { get; set; }

        public int SuppressTaxIdInLoop2310DAnd2420C { get; set; }

        public int SubmitPtPmts { get; set; }

        public int SuppressPrvInLoop2310A { get; set; }

        public int SuppressRendProvUPIN { get; set; }

        public int SuppressSuperProvUPIN { get; set; }

        public int SuppressOrderProvUPIN { get; set; }

        public int SuppressPurchasedProvUPIN { get; set; }

        public int SuppressServiceFacilityUPIN { get; set; }

        public int SuppressTaxIdInLoop2310C { get; set; }

        public int SuppressTaxIdInLoop2310E { get; set; }

        public int SuppressTaxIdInLoop2420E { get; set; }

        [Required]
        [StringLength(10)]
        public string CO42MapCode { get; set; }

        public int X270SendTrnAlways { get; set; }

        public int X270CustomMapLegacy { get; set; }

        public int SendOtafInPrimary { get; set; }

        public int X270SendEbDateRange { get; set; }

        public int X270SendPrv { get; set; }

        public int X270SendEbAddlInfo { get; set; }

        public int BestMatchCpt { get; set; }

        public int DoNotAddSalesTax { get; set; }

        public int X270SendGroupNo { get; set; }

        public int X270SendRelationShip { get; set; }

        public int x270SendSSN { get; set; }

        public int x270SendInsuredN4 { get; set; }

        public int HPSAQualify { get; set; }

        public int PrintEPSDTDataInHcfa10d { get; set; }

        public int ConsiderToothNumberAsModifier { get; set; }

        public int IsAnesthesiaMinutesInHCFA { get; set; }

        public int EnrollmentReq { get; set; }

        [Required]
        [StringLength(20)]
        public string CountryCode { get; set; }

        [Required]
        [StringLength(60)]
        public string NDCFormat { get; set; }

        public int X270SendDoi { get; set; }

        public int X270SendDosAsDoi { get; set; }

        public int SendHyphenInTaxId { get; set; }

        public int PrimaryTimelyFiling { get; set; }

        public int SecondaryTimelyFiling { get; set; }

        public int IsServiceLocationAddressinLoop2010AA { get; set; }

        public int NoPrimaryOtafIn2400 { get; set; }

        public int StripLeadingZeroFromMoney { get; set; }

        public int SendMedicalRecordNumber { get; set; }

        public int StripTrailingZerosFromMoney { get; set; }

        [Required]
        [StringLength(40)]
        public string insuranceNameANSI { get; set; }

        [Required]
        [StringLength(40)]
        public string LiabilityCarrierCode { get; set; }

        public int DoNotAddFinanceChrg { get; set; }

        [Required]
        [StringLength(60)]
        public string EcwMasterId { get; set; }

        public int? CapitationType { get; set; }

        public int EnterpriseId { get; set; }

        public int DoNotChangeClmAssgnmntAfterCapAdj { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] ModifiedDate { get; set; }

        public int AdditionalMask { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InsuranceDetail> InsuranceDetails { get; set; }
    }
}
