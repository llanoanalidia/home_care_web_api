﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("Demographic")]
    public class Demographic
    {
            public Demographic()
            {
                //Patients = new HashSet<Patient>();
            }

            [Key]
            public int IdDemographic{ get; set; }

            [Required]
            [StringLength(100)]
            public string Adress1 { get; set; }

            [Required]
            [StringLength(100)]
            public string Adress2 { get; set; }

            [Required]
            [StringLength(100)]
            public string City { get; set; }

            [Required]
            [StringLength(100)]
            public string State { get; set; }

            [Required]
            [StringLength(100)]
            public string ZipCode { get; set; }

            [Required]
            [StringLength(100)]
            public string Country { get; set; }
             //public virtual ICollection<Patient> Patients { get; set; }
        }
    }

