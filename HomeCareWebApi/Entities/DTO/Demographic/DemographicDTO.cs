﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DTO.Demographic
{
    public class DemographicDTO
    {
      
        public string Adress1 { get; set; }
        public string Adress2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }

    }
}
