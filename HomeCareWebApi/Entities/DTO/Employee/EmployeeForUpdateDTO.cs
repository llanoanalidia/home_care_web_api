﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DTO.Employee
{
     public class EmployeeForUpdateDTO
    {
        public string FirstName { get; set; }
        public string Middle { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string Gender { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string EMail { get; set; }
        public string Status { get; set; }
        public int IdRole { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public UserForUpdateDTO User { get; set; }

    }
}
