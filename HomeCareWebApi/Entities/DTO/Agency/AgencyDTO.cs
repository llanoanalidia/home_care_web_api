﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DTO.Agency
{
   public class AgencyDTO
   {
      
        public int IdAgency { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string TelephoneNumber { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string URL { get; set; }
        public string SiteId { get; set; }
        public string FederalTaxID { get; set; }
        public string CliaId { get; set; }
        public string ParentTIN { get; set; }
        public string GroupNPI { get; set; }
        public string TaxonomyCode { get; set; }
        public string LockboxNo { get; set; }
        public int DeleteFlag { get; set; }
    }
}
