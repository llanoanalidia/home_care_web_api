﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.DTO
{
   public class UserForUpdateDTO
    {
        [Required]
        [StringLength(60, ErrorMessage = "Name can't be longer than 60 characters")]
        public string UserName { get; set; }

        [Required]
        [StringLength(60)]
        [Display(Name = "PassWord")]
        public string Pasword { get; set; }
       // public int RoleId { get; set; }
    }
}
