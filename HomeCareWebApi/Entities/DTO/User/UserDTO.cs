﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.DTO
{
    public class UserDTO
    {

        public int UserId { get; set; }

        [Required]
        [StringLength(60)]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(60)]
        [Display(Name = "PassWord")]
        public string Pasword { get; set; }
    }
}
