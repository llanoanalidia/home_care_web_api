﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DTO.Doctor
{
    public class DoctorForUpdateDTO
    {
        public int IdDoctor { get; set; }
        public string FirstName { get; set; }
        public string Middle { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string eMail { get; set; }
        public string NPI { get; set; }
        public int IdUser { get; set; }
        public UserForUpdateDTO User { get; set; }
    }
}
