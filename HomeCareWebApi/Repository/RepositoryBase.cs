﻿using Entities;
using HomeCareContracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.SqlServer;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;


namespace Repository
{
    /*El repositorio base se utiliza para la implementacion de los metodos en comun que puedan tener las 
     diferentes entidades, esta clase hereda de la interfaz generica de IRepository*/
    public abstract class RepositoryBase<T> : IRepository<T> where T : class
    {
        private RepositoryContext context { get; set; }
        public RepositoryBase(RepositoryContext repositoryContext)
        {
            this.context = repositoryContext;
    
        }
        public void Create(T entity)
        {
            this.context.Set<T>().Add(entity);
       
        }

        public void Delete(T entity)
        {
            this.context.Set<T>().Remove(entity);
            
        }

        public IQueryable<T> FindAll()
        {
            
            return this.context.Set<T>().AsNoTracking();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this.context.Set<T>().Where(expression).AsNoTracking();
        }

        public void Update(T entity)
        {
            this.context.Set<T>().Update(entity);
        }

    }
}
