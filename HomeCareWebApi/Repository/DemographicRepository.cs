﻿using Entities;
using Entities.Models;
using HomeCareContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
   public class DemographicRepository: RepositoryBase<Demographic>, IDemographicRepository
    {
       public DemographicRepository(RepositoryContext context)
              : base(context)
        {
            
        }
        public void CreateDemographic(Demographic demographic)
        {
            Create(demographic);
        }

        public void DeleteDemographic(Demographic demographic)
        {
            Delete(demographic);
        }

        public async Task<IEnumerable<Demographic>> GetAllDemographicsAsync()
        {
            return await FindAll()
                .OrderBy(demo => demo.City)
                .ToListAsync();
        }

        /*public async Task<Demographic> GetDemographicByIdAsync(int id)
        {

            return await FindByCondition(demo => demo.IdDemographic.Equals(id))
                    .Include(patient => patient.IdDemographic)
                    .FirstOrDefaultAsync();
        }*/

       /* public async Task<Demographic> GetDemographicWhitDetailsAsync(int id)
        {
            return await FindByCondition(demo => demo.IdDemographic.Equals(id)).FirstOrDefaultAsync();
        }*/

        public void UpdateDemographic(Demographic demographic)
        {
            Update(demographic);
        }
    }
}
