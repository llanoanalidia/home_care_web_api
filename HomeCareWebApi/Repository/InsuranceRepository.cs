﻿using Entities;
using Entities.Models;
using HomeCareContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class InsuranceRepository : RepositoryBase<Insurance>, IInsuranceRepository
    {
        public InsuranceRepository(RepositoryContext context) 
            :base(context)
        {
        
        }

        public void CreateInsurance(Insurance Insurance)
        {
            Create(Insurance);
        }

        public void DeleteInsurance(Insurance Insurance)
        {
            Delete(Insurance);
        }

        public async Task<IEnumerable<Insurance>> GetAllInsurancesAsync()
        {
            return await FindAll()
                .OrderBy(ins => ins.Address1)
                .ToListAsync();
        }

        public async Task<Insurance> GetInsuranceByIdAsync(int id)
        {
            return await FindByCondition(ins => ins.InsuranceId.Equals(id))
                    .Include(insd => insd.InsuranceDetails)
                    .FirstOrDefaultAsync();
        }

        public async Task<Insurance> GetInsuranceWhitDetailsAsync(int id)
        {
            return await FindByCondition(ins => ins.InsuranceId.Equals(id)).FirstOrDefaultAsync();
        }

        public void UpdateInsurance(Insurance Insurance)
        {
            Update(Insurance);
        }
    }
}
