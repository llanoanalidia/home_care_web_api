﻿using Entities;
using Entities.Models;
using HomeCareContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
   public class EmployeeRerpository : RepositoryBase<Employee>, IEmployeeRepository
    {
        private DbContext _context;
        public EmployeeRerpository(RepositoryContext context)
        : base(context)
        {
            _context = context;
        }

        public void CreateEmployee(Employee employee)
        {
            this._context.Database.ExecuteSqlRaw("EXECUTE dbo.Insert_Employee {0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18}",
                                                 employee.User.UserName,
                                                 employee.User.Pasword,
                                                 employee.User.RoleId,
                                                 employee.User.State,
                                                 employee.FirstName,
                                                 employee.Middle,
                                                 employee.LastName,
                                                 employee.DateOfBirth,
                                                 employee.SocialSecurityNumber,
                                                 employee.Gender,
                                                 employee.Phone1,
                                                 employee.Phone2,
                                                 employee.EMail,
                                                 employee.Status,
                                                 employee.Address1,
                                                 employee.Address2,
                                                 employee.City,
                                                 employee.State,
                                                 employee.ZipCode);
        }

        public void DeleteEmployee(Employee employee)
        {
            Delete(employee);
        }

        public async Task<IEnumerable<Employee>> GetAllEmployeesAsync()
        {
            return await FindAll()
                .OrderBy(emp => emp.FirstName)
                .ToListAsync();
        }

        public async Task<Employee> GetEmployeeByIdAsync(int id)
        {
            return await FindByCondition(doc => doc.IdEmployees.Equals(id))
                             .Include(usr => usr.User)
                             .FirstOrDefaultAsync();
        }

        public Task<Employee> GetEmployeeWhitDetailsAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<Employee> GetUserWhitDetailsAsync(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateEmployee(Employee employee)
        {
            Update(employee);
        }
    }
}
