﻿using Entities;
using Entities.Models;
using HomeCareContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Repository
{
    public class DoctorRepository : RepositoryBase<Doctor>, IDoctorRepository
    {
        private DbContext _context;
        public DoctorRepository(RepositoryContext context)
        : base(context)
        {
            _context = context;
        }

        public void CreateDoctor(Doctor doctor)
        {
            this._context.Database.ExecuteSqlRaw("EXECUTE dbo.Insert_Doctor {0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}",
                                                doctor.User.UserName,
                                                doctor.User.Pasword,
                                                doctor.User.RoleId,
                                                doctor.User.State,
                                                doctor.FirstName,
                                                doctor.Middle,
                                                doctor.LastName,
                                                doctor.DateOfBirth,
                                                doctor.Phone1,
                                                doctor.Phone2,
                                                doctor.eMail, 
                                                doctor.NPI);
        }

        public void DeleteDoctor(Doctor doctor)
        {
            Delete(doctor);
        }

        public async Task<IEnumerable<Doctor>> GetAllDoctorsAsync()
        {
            return await FindAll()
                .OrderBy(doc => doc.FirstName)
                .ToListAsync();
        }

        public async Task<Doctor> GetDoctorByIdAsync(int id)
        {

            return await FindByCondition(doc => doc.IdDoctor.Equals(id))
                    .Include(usr => usr.User)
                    .FirstOrDefaultAsync();
        }

        public async Task<Doctor> GetDoctorWhitDetailsAsync(int id)
        {
            return await FindByCondition(doc => doc.IdDoctor.Equals(id)).FirstOrDefaultAsync();
        }

        public void UpdateDoctor(Doctor doctor)
        {
            Update(doctor);
        }
    }
}
