﻿using Entities;
using Entities.Models;
using HomeCareContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class InsuranceDetailRepository : RepositoryBase<InsuranceDetail>, IInsuranceDetailRepository
    {
        public InsuranceDetailRepository(RepositoryContext context) 
                :base(context)
        {
        }

        public void CreateInsuranceDetail(InsuranceDetail insuranceDetail)
        {
            Create(insuranceDetail);
        }

        public void DeleteInsuranceDetail(InsuranceDetail insuranceDetail)
        {
            Delete(insuranceDetail);
        }

        public async Task<IEnumerable<InsuranceDetail>> GetAllInsuranceDetailsAsync()
        {
            return await FindAll()
              .OrderBy(insd => insd.AssignBenefits)
              .ToListAsync();
        }

        public async Task<InsuranceDetail> GetInsuranceDetailByIdAsync(int id)
        {
            return await FindByCondition(insd => insd.IdInsuranceDetail.Equals(id))
                     .Include(ins => ins.IdInsurance)
                     .FirstOrDefaultAsync();
        }

        public async Task<InsuranceDetail> GetInsuranceDetailWhitDetailsAsync(int id)
        {
            return await FindByCondition(insd => insd.IdInsuranceDetail.Equals(id)).FirstOrDefaultAsync();
        }

        public void UpdateInsuranceDetail(InsuranceDetail insuranceDetail)
        {
            Update(insuranceDetail);
        }
    }
}
