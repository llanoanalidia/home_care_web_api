﻿using Entities;
using Entities.Models;
using HomeCareContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
   public class UserRepository : RepositoryBase <User>, IUserRepository
    {
        public UserRepository(RepositoryContext context)
           : base(context)
        {

        }
        public void CreateUser(User user)
        {
            Create(user);
        }
        public void DeleteUser(User user)
        {
            Delete(user);
        }
        public async Task<IEnumerable<User>> GetAllUsersAsync() 
        {
            return await FindAll()
                .OrderBy(us => us.UserName)
                .ToListAsync();
        }
        public async Task<User> GetUserWhitDetailsAsync(int id)
        {
            return await FindByCondition(us=> us.UserId.Equals(id))
                    .Include(ac => ac.RoleId)
                    .FirstOrDefaultAsync();
        }
        public void UpdateUser(User user)
        {
            Update(user);
        }
        async Task<User> IUserRepository.GetUserByIdAsync(int id)
        {
            return await FindByCondition(user => user.UserId.Equals(id)).FirstOrDefaultAsync();
        }
    }
}
