﻿using Entities;
using Entities.Models;
using HomeCareContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class PatientRepository : RepositoryBase<Patient>, IPatientRepository
    {
        private DbContext _context;
        public PatientRepository(RepositoryContext context)
          : base(context)
        {
            _context = context;
        }
         
        public void CreatePatient(Patient patient)
        {

            this._context.Database.ExecuteSqlRaw("EXECUTE dbo.Insert_Patient {0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20}",
                                                  patient.User.UserName,
                                                  patient.User.Pasword,
                                                  patient.User.RoleId,
                                                  patient.User.State,
                                                  patient.FirstName,
                                                  patient.Middle,
                                                  patient.LastName,
                                                  patient.DateOfBirth,
                                                  patient.SocialSecurityNumber,
                                                  patient.Gender,
                                                  patient.Phone1,
                                                  patient.Phone2,
                                                  patient.eMail,
                                                  patient.Status,
                                                  patient.PatientCode,
                                                  patient.Demographic.Adress1,
                                                  patient.Demographic.Adress2,
                                                  patient.Demographic.City,
                                                  patient.Demographic.State,
                                                  patient.Demographic.ZipCode,
                                                  patient.Demographic.Country);
        }
        public void DeletePatient(Patient patient)
        {
            Delete(patient);
        }

        public  PagedList<Patient> GetAllPatientsAsync(PatientParameters patientParameters)
        {
            return  PagedList<Patient>.ToPagedList(FindAll().OrderBy(on => on.FirstName),
                patientParameters.PageNumber,
                patientParameters.PageSize);
        }

        public async Task<Patient> GetPatienByIdAsync(int id) { 
            return await FindByCondition(patient => patient.IdPatient.Equals(id))
                    .Include(usr => usr.User)
                    .Include(Demo => Demo.Demographic)
                    .FirstOrDefaultAsync();
        }

        public async Task<Patient> GetPatientWhitDetailsAsync(int id)
        {
            return await FindByCondition(patient => patient.IdPatient.Equals(id)).FirstOrDefaultAsync();
        }

        public void UpdatePatient(Patient patient)
        {
            Update(patient);
        }
    }
}
