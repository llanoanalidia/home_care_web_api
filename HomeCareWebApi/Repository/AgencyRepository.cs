﻿using Entities;
using Entities.Models;
using HomeCareContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class AgencyRepository : RepositoryBase<Agency>, IAgencyRepository
    {

        public AgencyRepository(RepositoryContext context)
            : base(context)
        { 
        
        }

        public void CreateAgency(Agency agency)
        {
            Create(agency);
        }

        public void DeleteAgency(Agency agency)
        {
            Delete(agency);
        }

        public async Task<Agency> GetAgencyByIdAsync(int id)
        {
            return await FindByCondition(agen => agen.IdAgency.Equals(id))
                    .Include(insu => insu.SiteId)
                    .FirstOrDefaultAsync();
        }

        public Task<Agency> GetAgencyWhitDetailsAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Agency>> GetAllAgencysAsync()
        {
            return await FindAll()
                .OrderBy(agen => agen.Name)
                .ToListAsync();
        }

        public void UpdateAgency(Agency agency)
        {
            throw new NotImplementedException();
        }
    }
}
